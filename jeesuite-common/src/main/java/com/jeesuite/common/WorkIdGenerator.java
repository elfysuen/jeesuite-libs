package com.jeesuite.common;

public interface WorkIdGenerator {

	public int generate(String nodeId);
}
